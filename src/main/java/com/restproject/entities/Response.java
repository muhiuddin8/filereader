package com.restproject.entities;

public class Response {

	private String msisdn;
	private String message;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Response [msisdn=" + msisdn + ", message=" + message + "]";
	}

}
