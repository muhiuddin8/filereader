package com.restproject.entities;

public class Requests {
	private String msisdn;
	private String message;
	private String opertor;
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getOpertor() {
		return opertor;
	}
	public void setOpertor(String opertor) {
		this.opertor = opertor;
	}
	@Override
	public String toString() {
		return "Requests [msisdn=" + msisdn + ", message=" + message + "]";
	}
	

}
