package com.restproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.restproject.entities.RequestApi;
import com.restproject.entities.Requests;
import com.restproject.entities.Response;


@RestController
@RequestMapping("/simpaisa")
public class SimPaisaController {

	@Autowired
    RestTemplate restTampate;
	
	@RequestMapping(value="/sms/send" , method =RequestMethod.POST )
	ResponseEntity<?> restCall(@RequestBody Requests requests) {
		 
		
		 
		 Response response=null;
		 
		 String opertor = requests.getOpertor();
		 String url="";
		 if(opertor.contains("zong") || opertor.contains("ZONG")) {
			  url="http://192.168.9.9:8881/simpaisa/sms/send";
		 }else if (opertor.contains("mobilink") || opertor.contains("MOBILINK")) {
			 url="http://192.168.9.9:8883/simpaisa/sms/send"; 
		 }
		 
		 RequestApi requestApi = new RequestApi();
		 requestApi.setMessage(requests.getMessage());
		 requestApi.setMsisdn(requests.getMsisdn());
		 
	  try {
		
	 	
   //  String urlMobilink = "http://192.168.9.9:8883/simpaisa/sms/send";	//mobilink
   //  String urlZong="http://192.168.9.9:8881/simpaisa/sms/send";
      response  = restTampate.postForObject(url,requestApi,Response.class);
	}catch(Exception e ) {
		e.printStackTrace();
	}
    
    return new ResponseEntity<Response>(response,HttpStatus.OK);
		
	
	}
}
