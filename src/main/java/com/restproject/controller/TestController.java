package com.restproject.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.restproject.entities.Response;

@RestController
@RequestMapping("/test")
public class TestController {

	
	@RequestMapping("/sms/send")
	ResponseEntity<?> checkTest(@RequestBody Response response){
		
	   
		response.setMsisdn("455578885555599");
		response.setMessage("success ");
		
		return new ResponseEntity<Response>(response,HttpStatus.OK);
		
		
		
	}
}
