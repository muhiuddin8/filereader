package com.restproject.filreader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import com.restproject.entities.RequestApi;
public class FileReaderNetworks {

	private final static Logger log = LoggerFactory.getLogger(FileReaderNetworks.class);

	// this Method is for moving Files from one place to another
	public static void movingFiles(String fileName) {

		try {

			Path temp = Files.move(Paths.get("F:/filefolder/files/" + fileName),
					Paths.get("F:/filefolder/files/readFiles/" + fileName));

			if (temp != null) {
				//System.out.println(" moved successfully");
				log.info(fileName+"Moved Successfully");
			} else {
				//System.out.println("Failed to move the file");
				log.info(fileName+"Failed to move ");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String args[]) {

		String[] splited=null;
		  
	      
	      int count=0;
		try {
			
			FileWriter myWriter = new FileWriter("F:/filefolder/files/success/success22.txt");
		      
		     // myWriter.close();
            String line="";
			
			File directoryPath = new File("F:/filefolder/files");
            
			// List all files and directories
			System.out.println("------------All files------------");
			for (File file : directoryPath.listFiles()) {
				System.out.println(file.getName());
			}

			// System.out.println("\n------------Text files------------");
			File[] files = directoryPath.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith(".log");
					
					//return name.endsWith(".log");
				}
			});
			String getFileName = "";
			
			for (File fileLoopReader : files) {
				
				getFileName = fileLoopReader.getName();
				FileReader fr = new FileReader(fileLoopReader);
			 BufferedReader bufferreader = new BufferedReader(fr);
			 String readData="";
		     String test="";
		     String values[]=null;
			 
			 
			 while ((line = bufferreader.readLine()) !=null ) {     
		      
				 readData=readData.concat(line);
				 if(line.contains("ZONG_DEDUCTBALANCE_REQUEST") || line.contains("ZONG_DEDUCTBALANCE_RESPONSE") ) {
					 
					 
					 readData=readData.concat(line);
					 if(line.contains("ZONG_DEDUCTBALANCE_REQUEST")) {
						
						 
						 
						 splited  = line.split("\\DEDUCTBALANCE:");
						 splited[1].concat(",");
						 test=splited[1].replace("=", ",");
						 
						 values= test.split("\\,");
						 
					 } else {
					// continue ;
                     
					 
					 if(line.contains("successfully")) {
						 
						
						 count++;
						 myWriter.write(values[1] +" "+values[3]);   // write in file
						 myWriter.write(System.getProperty("line.separator"));
						 //
						 
						// readData = readData.substring(0, readData.indexOf("@@"));
					 }
					 }//else
				 }
				 
					
									readData="";
									splited=null;
										
				} 
                System.out.println("success"+count);
				fr.close();
				//log.info("moving file name=====> "+getFileName);
				// System.out.println("file name to move==="+getFileName);
				FileReaderNetworks.movingFiles(getFileName);
		}
			myWriter.close();
		} // files Loop end..
		catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
		}
	}

}
