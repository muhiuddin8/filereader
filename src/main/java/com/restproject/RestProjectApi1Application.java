package com.restproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.restproject.filreader.FileReaderNetworks;

@SpringBootApplication

public class RestProjectApi1Application {

	public static void main(String[] args) {
		SpringApplication.run(RestProjectApi1Application.class, args);	
		FileReaderNetworks.main(args);
		
		
	}

}
